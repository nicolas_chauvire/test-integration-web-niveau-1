# TEST D’INTEGRATION WEB NIVEAU 1

* Le test suivant a pour but de connaitre votre niveau de connaissance sur les compétences suivantes :
     -	HTML 5
     -	CSS 3
     -	Utilisation des plugins (Bootstrap, jQuery)

###Consigne :

   * Vous devez réaliser une page web statique responsive (sans données ni liens).
   * Cette interface doit être utilisable sur les 3 supports suivants:
       -	Pc
       -	Tablette > 8 pouces
       -	Smartphone 5 pouces (environs).

   * Vous trouverez en annexe les maquettes avec le rendu attendu sur chaque support.
   * Cette interface doit comporter plusieurs éléments distincts :
       - 1 barre horizontale fixe en haut avec un logo et un bouton « déconnecter ».
       - 1 menu vertical à gauche rétractable à gauche seulement sur mobile et tablette (l’effet rétractable est optionnel).
       - 3 panels (contenu « lorem ipsum »).
       - 1 Tabs avec 2 boutons (contenu « Lorem ipsum» ).
   * Vous êtes libre dans le choix des couleurs.
   * Pour réaliser ce test vous avez besoin d’un serveur web, d’un éditeur de texte et de GIT.

### Get started :

   * 1 - fork et cloner votre dépôt sur votre serveur.
   * 2 - réaliser le test en modifiant les fichiers clonées.
   
   * Voici les liens pour accéder à la documentation de chaque plugin.
       - Jquery : 	http://jquery.com/
       - Boostrap :	http://getbootstrap.com/
       - Sidr : 	https://www.berriart.com/sidr/

   * Une fois votre test terminé (selon vos connaissances) vous devez faire un zip de votre projet et l'envoyer par https://wetransfer.com/ à n.chauvire@2isr.fr

### Maquettes PC :
![maquette pc.PNG](https://bitbucket.org/repo/XXX9ARo/images/4150677708-maquette%20pc.PNG)
### Maquette Tablette : 
![maquette tablette.PNG](https://bitbucket.org/repo/XXX9ARo/images/1878497701-maquette%20tablette.PNG)
### Maquette Smartphone : 
![maquette smartphone.PNG](https://bitbucket.org/repo/XXX9ARo/images/4241592406-maquette%20smartphone.PNG)